<?php
class WATP {
    /**
     * Constructor
     */
    public function __construct() {  
        /**
         * Hook: Register additional taxonomy on woocommerce product add/edit
         */
        add_action('init', array( $this, 'register_additional_taxonomy' ));
        
        /**
         * Register Shortcode "special_categories" to show show special categories products
         */
        add_shortcode( 'special_categories', array( $this, 'show_special_categories_products') );
        
        /**
         * Hook: Add "special_categories" Shortcode to Visual Composer
         */
        add_action( 'init', array( $this, 'visual_composer_special_categories_products') );
    }  
    
    /**
     * Register additional taxonomy on woocommerce product add/edit
     */
    public function register_additional_taxonomy(){
        register_taxonomy("special-categories", array("product"), array("hierarchical" => true, "label" => "Special Categories", "singular_label" => "Special Category", "rewrite" => true));
    }
    
    /**
     * Show "special_categories" to show show special categories products
     * @global type $wpdb
     * @param type $atts
     * @return type
     */
    public function show_special_categories_products($atts){
        if($atts['ids']){
            
            global $wpdb;
            $productID=array();
            
            //Break special category ids into an array 
            $specialCategoryIds=explode(',', $atts['ids']);            
            
            foreach((array)$specialCategoryIds as $specialCategoryId){                
                $getProductIDs = $wpdb->get_results( 'SELECT '.$wpdb->prefix.'posts.ID FROM '.$wpdb->prefix.'term_taxonomy INNER JOIN '.$wpdb->prefix.'term_relationships ON '.$wpdb->prefix.'term_taxonomy.term_taxonomy_id = '.$wpdb->prefix.'term_relationships.term_taxonomy_id INNER JOIN '.$wpdb->prefix.'posts ON '.$wpdb->prefix.'term_relationships.object_id = '.$wpdb->prefix.'posts.ID WHERE '.$wpdb->prefix.'term_taxonomy.term_id = '.$specialCategoryId.' AND '.$wpdb->prefix.'posts.post_status = "publish" AND '.$wpdb->prefix.'posts.post_type = "product"', OBJECT );
                foreach($getProductIDs as $getProductID){
                    $productID[]=$getProductID->ID;
                }
            }
            
            //Remove Duplicate Product IDs
            $productID=array_unique($productID);
            
            //Join product id array elements with a string
            $productIDs=implode(", ",$productID);
            
            return do_shortcode('[products ids="'.$productIDs.'"]');
        }
    }
    
    /**
     * Add "special_categories" Shortcode to Visual Composer
     */
    public function visual_composer_special_categories_products(){        
        if(function_exists('vc_map')){
            add_shortcode ('wc_special_product_categories', function ( $atts, $content = null ) {                
                $content = wpb_js_remove_wpautop($content, false);
                $output = '<div class="wc-special-product-categories-contain">';                
                $output .= do_shortcode('[special_categories ids="'.$content.'"]');
                $output .= '</div>';
                return $output;
            });

            vc_map( array(
                "name" => __("WooCommerce Special Product Categories"),
                "base" => "wc_special_product_categories",
                "class" => "",
                "icon" => "icon-wpb-woocommerce",
                "category" => __('WooCommerce'),
                "params" => array(
                    array(
                            "type" => "autocomplete",                            
                            "class" => "",
                            "heading" => __("Special Category IDs", "js_composer"),
                            "param_name" => "content",
                            "settings" => array(
                                'multiple' => true,
                                'sortable' => true,
                                'unique_values' => true,
                                'values' => $this->get_categories('special-categories')    
                            ),
                            "description" => __("Enter List of Special Categories")
                    )
                )
            ));
        }
    }
    
    /**
     * Get Categories
     * @global type $wpdb
     * @param type $taxonomy
     * @return string
     */
    public function get_categories($taxonomy){
        global $wpdb;
        $getTerms=$wpdb->get_results("SELECT ".$wpdb->prefix."term_taxonomy.term_id, ".$wpdb->prefix."terms.`name` FROM ".$wpdb->prefix."term_taxonomy INNER JOIN ".$wpdb->prefix."terms ON ".$wpdb->prefix."term_taxonomy.term_id = ".$wpdb->prefix."terms.term_id WHERE ".$wpdb->prefix."term_taxonomy.taxonomy = '{$taxonomy}'");
        
        $countTerms=0;
        $terms=array();
        foreach($getTerms as $getTerm){
            $terms[$countTerms]['value']=$getTerm->term_id;
            $terms[$countTerms]['label']=$getTerm->name." - ID: ".$getTerm->term_id;
            $countTerms++;
        }
        return $terms;
    }
}