<?php
/*
  Plugin Name: WooCommerce Additional Taxonomy on Product
  Plugin URI: http://codup.io/
  Description: WooCommerce Additional Taxonomy on Product
  Version: 1.0
  Author Name: Codup
  Author URI: http://codup.io/
 */
if (!defined('DS')) {
    define("DS", DIRECTORY_SEPARATOR);
}
if (!defined('WP_MEMORY_LIMIT')) {
    define('WP_MEMORY_LIMIT', "1024M");
}
if (!defined('VERSION')) {
    define("VERSION", '1.0');
}

include(dirname(__FILE__) . DS . 'includes' . DS . 'class.watp.php');
$WATP = new WATP();  